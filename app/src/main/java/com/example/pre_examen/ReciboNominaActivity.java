package com.example.pre_examen;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class ReciboNominaActivity extends AppCompatActivity {
    private TextView tvNombreTrabajador;
    private EditText etNumRecibo, etNombre, etHorasNormales, etHorasExtras, etSubtotal, etImpuesto, etTotalPagar;
    private RadioGroup radioGroupPuesto;
    private RadioButton radioAuxiliar, radioAlbanil, radioIngObra;
    private Button btnCalcular, btnLimpiar, btnRegresar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibo_nomina);

        tvNombreTrabajador = findViewById(R.id.tvNombreTrabajador);
        etNumRecibo = findViewById(R.id.etNumRecibo);
        etNombre = findViewById(R.id.etNombre);
        etHorasNormales = findViewById(R.id.etHorasNormales);
        etHorasExtras = findViewById(R.id.etHorasExtras);
        radioGroupPuesto = findViewById(R.id.radioGroupPuesto);
        radioAuxiliar = findViewById(R.id.radioAuxiliar);
        radioAlbanil = findViewById(R.id.radioAlbanil);
        radioIngObra = findViewById(R.id.radioIngObra);
        etSubtotal = findViewById(R.id.etSubtotal);
        etImpuesto = findViewById(R.id.etImpuesto);
        etTotalPagar = findViewById(R.id.etTotalPagar);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);

        // Recibe el nombre del trabajador desde la MainActivity
        Intent intent = getIntent();
        String nombreTrabajador = intent.getStringExtra("nombreTrabajador");
        tvNombreTrabajador.setText(nombreTrabajador);

        btnCalcular.setOnClickListener(v -> {
            if (validarCampos()) {
                calcularRecibo();
            }
        });
        btnLimpiar.setOnClickListener(v -> limpiarCampos());
        btnRegresar.setOnClickListener(v -> finish());
    }

    private boolean validarCampos() {
        if (tvNombreTrabajador.getText().toString().isEmpty()) {
            Toast.makeText(this, "El nombre del trabajador es obligatorio", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (etNumRecibo.getText().toString().isEmpty()) {
            etNumRecibo.setError("Este campo es obligatorio");
            return false;
        }
        if (etNombre.getText().toString().isEmpty()) {
            etNombre.setError("Este campo es obligatorio");
            return false;
        }
        if (etHorasNormales.getText().toString().isEmpty()) {
            etHorasNormales.setError("Este campo es obligatorio");
            return false;
        }
        if (etHorasExtras.getText().toString().isEmpty()) {
            etHorasExtras.setError("Este campo es obligatorio");
            return false;
        }
        if (radioGroupPuesto.getCheckedRadioButtonId() == -1) {
            Toast.makeText(this, "Seleccione un puesto", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private void calcularRecibo() {
        int numRecibo = Integer.parseInt(etNumRecibo.getText().toString());
        String nombre = etNombre.getText().toString();
        int horasNormales = Integer.parseInt(etHorasNormales.getText().toString());
        int horasExtras = Integer.parseInt(etHorasExtras.getText().toString());
        int puesto = getPuestoSeleccionado();

        ReciboNomina recibo = new ReciboNomina(numRecibo, nombre, horasNormales, horasExtras, puesto, 0.16);

        double subtotal = recibo.calcularSubtotal();
        double impuesto = recibo.calcularImpuesto();
        double totalPagar = recibo.calcularTotalPagar();

        etSubtotal.setText(String.format("%.2f", subtotal));
        etImpuesto.setText(String.format("%.2f", impuesto));
        etTotalPagar.setText(String.format("%.2f", totalPagar));
    }

    private int getPuestoSeleccionado() {
        int selectedId = radioGroupPuesto.getCheckedRadioButtonId();
        if (selectedId == radioAuxiliar.getId()) {
            return 1;
        } else if (selectedId == radioAlbanil.getId()) {
            return 2;
        } else if (selectedId == radioIngObra.getId()) {
            return 3;
        }
        return 1; // Por defecto
    }

    private void limpiarCampos() {
        etNumRecibo.setText("");
        etNombre.setText("");
        etHorasNormales.setText("");
        etHorasExtras.setText("");
        radioGroupPuesto.clearCheck();
        etSubtotal.setText("");
        etImpuesto.setText("");
        etTotalPagar.setText("");
    }
}