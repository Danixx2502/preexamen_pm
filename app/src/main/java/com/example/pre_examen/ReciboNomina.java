package com.example.pre_examen;

public class ReciboNomina {
    private int numRecibo;
    private String nombre;
    private int horasNormales;
    private int horasExtras;
    private int puesto;
    private double porcentajeImpuesto;

    public ReciboNomina(int numRecibo, String nombre, int horasNormales, int horasExtras, int puesto, double porcentajeImpuesto) {
        this.numRecibo = numRecibo;
        this.nombre = nombre;
        this.horasNormales = horasNormales;
        this.horasExtras = horasExtras;
        this.puesto = puesto;
        this.porcentajeImpuesto = porcentajeImpuesto;
    }

    public double calcularSubtotal() {
        double pagoBase = 200;
        switch (puesto) {
            case 1:
                pagoBase += 200 * 0.20;
                break;
            case 2:
                pagoBase += 200 * 0.50;
                break;
            case 3:
                pagoBase += 200 * 1.00;
                break;
        }
        return (pagoBase * horasNormales) + (pagoBase * horasExtras * 2);
    }

    public double calcularImpuesto() {
        return calcularSubtotal() * 0.16;
    }

    public double calcularTotalPagar() {
        return calcularSubtotal() - calcularImpuesto();
    }

    // Getters y Setters
    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getHorasNormales() {
        return horasNormales;
    }

    public void setHorasNormales(int horasNormales) {
        this.horasNormales = horasNormales;
    }

    public int getHorasExtras() {
        return horasExtras;
    }

    public void setHorasExtras(int horasExtras) {
        this.horasExtras = horasExtras;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public double getPorcentajeImpuesto() {
        return porcentajeImpuesto;
    }

    public void setPorcentajeImpuesto(double porcentajeImpuesto) {
        this.porcentajeImpuesto = porcentajeImpuesto;
    }
}