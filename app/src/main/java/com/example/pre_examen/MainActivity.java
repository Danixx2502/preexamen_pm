package com.example.pre_examen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    private EditText etNombreTrabajador;
    private Button btnEntrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etNombreTrabajador = findViewById(R.id.etNombreTrabajador);
        btnEntrar = findViewById(R.id.btnEntrar);

        btnEntrar.setOnClickListener(v -> {
            String nombreTrabajador = etNombreTrabajador.getText().toString();
            if (validarNombreTrabajador(nombreTrabajador)) {
                Intent intent = new Intent(MainActivity.this, ReciboNominaActivity.class);
                intent.putExtra("nombreTrabajador", nombreTrabajador);
                startActivity(intent);
            }
        });
    }

    private boolean validarNombreTrabajador(String nombreTrabajador) {
        if (nombreTrabajador.isEmpty()) {
            etNombreTrabajador.setError("El nombre del trabajador es obligatorio");
            return false;
        }
        return true;
    }
}